var BASE_URL			= "https://developer.apple.com";
var $allContent 		= $(".all-content .collection-focus-groups .collection-focus-group");
var numberOfSections 	= $allContent.length;
var sectionVideos 		= new Array();
var currId				= 0;
var year				= $(".collection-title").text().replace(/([A-Z])\w+/g, "").trim();$(".collection-title").text()

for (var i = 0; i < numberOfSections ; i++) {
	
	var vidCat 	= $allContent.find(".sticky section.grid section.row > section a > span").eq(i).text();

	$allContent.eq(i).find(".collection-item").each(function(ii, obj) {

		var $baseVidDomObj = $(obj).find(".grid > .row > section");
		
		var vidTitle 	= 	$baseVidDomObj.find("a").text();
		var vidSession 	= 	parseInt($baseVidDomObj.find(".video-tag.event > span").text().replace('Session ', ""));
		var vidURL		=	BASE_URL + $baseVidDomObj.find("a").attr("href");
		var vidPlatform	=	$baseVidDomObj.find(".video-tag.focus > span").text().split(",");
		$(vidPlatform).each(function(iii, obj){
			vidPlatform[iii] = vidPlatform[iii].trim();
		});

		var vidDesc		= 	$baseVidDomObj.find(".description ").text();

		

		//console.log(id, vidDesc, vidURL, vidPlatform, vidSession, vidTitle, year);

		sectionVideos.push({
			"id" : currId++,
			"year" : year,
			"cat" : vidCat,
			"title": vidTitle,
			"session" : vidSession,
			"url" : vidURL,
			"platform": vidPlatform,
			"desc": vidDesc
		});

	});

}


console.log(JSON.stringify(sectionVideos));
console.log("length", sectionVideos.length);