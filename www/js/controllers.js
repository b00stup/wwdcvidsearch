angular.module('wwdcvideoapp.controllers', ['ionic', "ui.router"])
//////////////////////////
//////////////////////////
//                      //
//      CONTROLLERS     //
//                      //
//////////////////////////

  //////////////////////////
  //      ListCtrl        //
  //////////////////////////
.controller('ListCtrl', function($rootScope, $scope, videos, SearchModel, SearchForm){
  // console.log('this ListCtrl speaking');

  SearchModel.setSearchModelForScope($scope);
  $scope.videos = videos;  
  $scope.openSearchModal = function() {
    SearchForm.show();
  }
})

  //////////////////////////
  //      SearchFormCtrl  //
  //////////////////////////
.controller('SearchFormCtrl', function($rootScope, $scope, SearchModel, SearchForm, VideoCatalog){
  // console.log('this SearchFormCtrl speaking');
  SearchModel.setSearchModelForScope($scope);
  $scope.closeSearchForm = function() {
    SearchForm.hide();
  }

  $scope.removeFilter = function(keyname) {
    //alert("key to delete: " + keyname);
    $scope.searchModel[keyname] = "";
  }

  $scope.yearMenuOptions = [{code:"", label:"any"}, 
                              {code:2012, label:2012}, 
                              {code:2013, label:2013}, 
                              {code:2014, label:2014}, 
                              {code:2015, label:2015}
                            ];
  $scope.platformMenuOptions = [{code:"", label:"any"}, 
                                {code:"iOS", label:"iOS"}, 
                                {code:"OS X", label:"OS X"}, 
                                {code:"watchOS", label:"watchOS"}, 
                                {code:"tvOS", label:"tvOS"}, 
                              ];
})

  //////////////////////////
  //      VideoDetailCtrl //
  //////////////////////////
.controller('VideoDetailCtrl', function($scope, videoModel, VideoCatalogMetadata){
  
  var vid = videoModel.id;

  function checkIsFavorite() {
    //console.log("checkIsFavorite")
    return VideoCatalogMetadata.isFavorite(vid);
  }
  
  function addToFavorites() {
    //console.log("adding to favorites")
    VideoCatalogMetadata.addToFavorites(vid)
  }
  
  function removeFromFavorites() {
    //console.log("removing from favorites")
    VideoCatalogMetadata.removeFromFavorites(vid)
  }   

  function checkIsViewed() {
    //console.log("checkIsViewed")
    return VideoCatalogMetadata.isViewed(vid);
  }  

  function addToViewed() {
    //console.log("adding to viewed")
    VideoCatalogMetadata.addToViewed(vid)
  }

  function removeFromViewed() {
    //console.log("removing from viewed")
    VideoCatalogMetadata.removeFromViewed(vid)
  }     

  //SCOPE DEFINITION
  $scope.videoModel = videoModel;

  $scope.goToAppleWebSite = function() {
    //1) open inAppBrowser videoModel.url
    window.open(videoModel.url, '_system', 'location=yes');
    //2) add to viewed
    addToViewed();
    return false;
  }

  $scope.favorite = {
    isOn : checkIsFavorite,
    onOn : addToFavorites,
    onOff : removeFromFavorites
  }
  
  $scope.viewed = {
    isOn : checkIsViewed,
    onOn : addToViewed,
    onOff : removeFromViewed
  }   

})

  //////////////////////////
  //      TabsCtrl  //
  //////////////////////////
.controller("TabsCtrl", function($scope, $state, $ionicTabsDelegate, SearchModel){

  SearchModel.setSearchModelForScope($scope);

  function selectTabWithIndex (index) {
    $ionicTabsDelegate.select(index);
  }  

  function resetState(){
    $state.go("videos");
  }

  $scope.setAsHome = function() {
    $scope.searchModel.favorite = undefined;
    $scope.searchModel.viewed   = undefined;
    selectTabWithIndex(0);
    resetState();
  }  

  $scope.setAsFavorite = function() {
    $scope.searchModel.favorite = true;
    $scope.searchModel.viewed   = undefined;
    selectTabWithIndex(1);    
    resetState();
  }

  $scope.setAsViewed = function() {
    $scope.searchModel.favorite = undefined;
    $scope.searchModel.viewed   = true;
    selectTabWithIndex(2);    
    resetState();
  }      

})