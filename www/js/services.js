angular.module('wwdcvideoapp.services', ['ionic', "ui.router"])
//////////////////////////
//////////////////////////
//                      //
//      SERVICES        //
//                      //
//////////////////////////
//////////////////////////

  //////////////////////////
  //      VideoCatalog    //
  //////////////////////////
.service('VideoCatalog', function($http, $q, $filter, $timeout) {

  var numberOffilteredVideos = 0;

  //var CATALOG_URL = "https://dl.dropboxusercontent.com/u/9750028/catalog.json";
  var CATALOG_URL = 'data/catalog.json';
  //var CATALOG_URL = 'data/smallCatalog.json';

  var self = this;

  self.videos = [];
  self.filtered = [];

  function error(data, status, headers, config) {
    //console.log("failure", data, status, headers, config);
    //console.log("video service failed.")
    dfd.reject(data);    
  }

  function getVideoJsonData() {

      var dfd = $q.defer();

      $http.get(CATALOG_URL)
        .success(function(data, status, headers, config) {

          //console.log("success", data, status, headers, config);
          //console.log("received data inside video service.")
          if(data) {
            dfd.resolve(self.videos = data);
          } else {
            error(data, status, headers, config, dfd);
          }
        })
        .error(function(data, status, headers, config) {
          error(data, status, headers, config, dfd);
        }) 

      return dfd.promise;
  }

  function allVideos() {

    var dfd = $q.defer();
    
    if(self.videos.length == 0) {
      getVideoJsonData().then(function(){
        dfd.resolve(self.videos);                 
      })
    } else {
      dfd.resolve(self.videos);           
    }
    return dfd.promise;
    
  }

  function filterVideos (query) {

    var dfd = $q.defer();

    allVideos().then(function(){
      dfd.resolve($filter('filter')(self.videos, query));
    });

    return dfd.promise;
   
  }

    // 'public' API for VideoCatalog
  return {
    getAll: allVideos,
    getFiltered: filterVideos,
    getNumFiltered: null
  }

})//END of VideoCatalog

  //////////////////////////
  //      SearchModel     //
  //////////////////////////
.service('SearchModel', function() {

  var searchCriteria = {
      cat: "",
      platform: "iOS",
      title: "What's new",
      year: "",
      session: "",
      desc: "",
      favorite: undefined,
      viewed: undefined
    }

  return { 
    data: searchCriteria,
    setSearchModelForScope: function(scope) {
      scope.searchModel = searchCriteria;
    }
  };  
})
  //////////////////////////
  //      SearchForm      //
  //////////////////////////
.service('SearchForm', function($rootScope, $ionicModal) {
  
  var self = this;

  $ionicModal.fromTemplateUrl('templates/search-form.html', {
    scope: $rootScope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    self.modal = modal
  })  

  self.show = function() {
    self.modal.show()
  }

  self.hide = function() {
    self.modal.hide();
  };

  return this;

})

  ///////////////////////////////////////////
  //    favorites and viewed video meta data   //
  ///////////////////////////////////////////
.factory('VideoCatalogMetadata', function(LocalStorage) {

  var STORAGE_KEY = "VideoCatalogMetadata";

  var self      = this;
  self.favorite = null;
  self.viewed   = null;

  function isArray(obj) {
    if(!isSet(obj)) return false;
    return obj instanceof Array
  }

  function isSet(val) {
    if(val == undefined || val == null) return false
    return true;
  }

  function retrieveFromStorage() {
    if( !(isArray(self.favorite)) 
        &&  !(isArray(self.viewed))
    ) {
      
      var dataObj = LocalStorage.getObject(STORAGE_KEY);

      if(dataObj && isArray(dataObj.favorite)) self.favorite = dataObj.favorite;
      else self.favorite = [];

      if(dataObj && isArray(dataObj.viewed)) self.viewed = dataObj.viewed;
      else self.viewed = [];
    }
  }

  function persistIntoStorage() {
    LocalStorage.setObject(STORAGE_KEY, { 
      favorite: self.favorite,
      viewed: self.viewed
    });
  }

  function isContained(needle, arr) {
    //avoid errors when there is no array...
    if (!isArray(arr)) return false;
    //otherwise, do this...
    return arr.indexOf(parseInt(needle)) > -1;
  }

  function removeFromArray(needle, arr) {
    if (!isArray(arr)) return false;
    var index = arr.indexOf(parseInt(needle));
    if(index > -1) {
      arr.splice(index, 1);
    }
  }

  function isFavorite (vid) {
    return isContained(vid, self.favorite);
  }

  function isViewed (vid) {
    return isContained(vid, self.viewed);
  } 

  function addVideoIdToFavorites(vid) {
    if(!isSet(vid)) return;
    if( isArray(self.favorite) && !isContained(vid, self.favorite) ) {
      self.favorite.push(vid);
      persistIntoStorage();
      //console.log("addVideoIdToFavorites", vid);
    }
  } 

  function removeVideoIdFromFavorites(vid) {
    if(!isSet(vid)) return;
    if( isArray(self.favorite) && isContained(vid, self.favorite) ) {
      removeFromArray(vid, self.favorite);
      persistIntoStorage();
      //console.log("removeVideoIdFromFavorites", vid);
    }
  }

  function addVideoIdToViewed(vid) {
    if(!isSet(vid)) return;
    if( isArray(self.viewed) && !isContained(vid, self.viewed) ) {
      self.viewed.push(vid);
      persistIntoStorage();
      //console.log("addVideoIdToViewed", vid);
    }
  }

  function removeVideoIdFromViewed(vid) {
    if(!isSet(vid)) return;
    if( isArray(self.viewed) && isContained(vid, self.viewed) ) {
      removeFromArray(vid, self.viewed);
      persistIntoStorage();
      //console.log("removeVideoIdFromViewed", vid);
    }
  }

  return {
    init: retrieveFromStorage,
    isFavorite: isFavorite,
    isViewed: isViewed,
    addToFavorites: addVideoIdToFavorites,
    removeFromFavorites: removeVideoIdFromFavorites,
    addToViewed: addVideoIdToViewed,
    removeFromViewed: removeVideoIdFromViewed
  }
})

  //////////////////////////
  //      LOCAL STORAGE    //
  //////////////////////////
.factory('LocalStorage', ['$window', function($window) {
  return {
    set: function(key, value) {
      $window.localStorage[key] = value;
    },
    get: function(key, defaultValue) {
      return $window.localStorage[key] || defaultValue;
    },
    setObject: function(key, value) {
      $window.localStorage[key] = JSON.stringify(value);
    },
    getObject: function(key) {
      return JSON.parse($window.localStorage[key] || '{}');
    }
  }
}])