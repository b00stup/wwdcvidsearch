angular.module('wwdcvideoapp.filters', ['ionic', "ui.router"])
//////////////////////////
//////////////////////////
//                      //
//      FILTERS         //
//                      //
//////////////////////////
//////////////////////////
.filter("platformFilter", function() {
  return function(videoItems, platform) {
    var results = [];
    if(videoItems && videoItems.length) {
      for (var i=0, l= videoItems.length; i < l; i++) {
        var currItem = videoItems[i];
        if(currItem && currItem.platform && currItem.platform.length) {
          if(currItem.platform.join().indexOf(platform) >= 0) {
            results.push(currItem);
          }
        }
      }
    }
    return results;
  };
})

.filter("formatPlatform", function() {
  return function(platformArr){
    return platformArr.join(", ");
  }
})

.filter("createGroupDividers", function() {
  return function(videoItems) {
    var oldYear = null;
    if(videoItems && videoItems.length) {
      for (var i=0, l= videoItems.length; i < l; i++) {
        var currItem = videoItems[i];
        
        if(oldYear != currItem.year) {
          currItem.divider = true;
          oldYear = currItem.year;
        }
      }
    }

    return videoItems
  }
}) 

.filter("favoriteFilter", function(VideoCatalogMetadata) {
  
  return function(videoItems, searchModelFavorite) {

    //if this is equal to undefined, 
    //it means that the favorites tab is not active
    //therefore cancelling this filter
    if(searchModelFavorite == undefined)
      return videoItems;

    var results = [];
    if(videoItems && videoItems.length) {
      for (var i=0, l= videoItems.length; i < l; i++) {
        var currItem = videoItems[i];

        if(VideoCatalogMetadata.isFavorite(currItem.id)) {
          results.push(currItem);
        }
      }
    }
    return results;
  }

})

.filter("viewedFilter", function(VideoCatalogMetadata) {
  
  return function(videoItems, searchModelViewed) {

    //if this is equal to undefined, 
    //it means that the viewed tab is not active
    //therefore cancelling this filter
    if(searchModelViewed == undefined)
      return videoItems;

    var results = [];
    if(videoItems && videoItems.length) {
      for (var i=0, l= videoItems.length; i < l; i++) {
        var currItem = videoItems[i];

        if(VideoCatalogMetadata.isViewed(currItem.id)) {
          results.push(currItem);
        }
      }
    }
    return results;
  }

})