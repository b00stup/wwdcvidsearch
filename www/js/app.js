// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('wwdcvideoapp', [
  "ionic", 
  "ui.router",
  "wwdcvideoapp.directives",
  "wwdcvideoapp.controllers",
  "wwdcvideoapp.services",
  "wwdcvideoapp.filters",
  "ngIOS9UIWebViewPatch"
  ]
)

//////////////////////////
//////////////////////////
//                      //
//      CONFIG          //
//                      //
//////////////////////////
//////////////////////////

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider, $sceDelegateProvider) {

  $ionicConfigProvider.backButton.text('');

  $sceDelegateProvider.resourceUrlWhitelist([
    'self',
    // Allow loading from our assets domain.  Notice the difference between * and **.
    'https://developer.apple.com/**']
  );
  
  $urlRouterProvider.otherwise("/");

  $stateProvider

    .state("videos", {
      url: '/',
      templateUrl: "templates/list.html",
      controller: "ListCtrl",
      resolve: {
        videos : function(VideoCatalog, VideoCatalogMetadata) {
          VideoCatalogMetadata.init();
          return VideoCatalog.getAll();
        }
      }      
    })   

    .state('detail', {
      url:'/detail/:vid',
      templateUrl: 'templates/detail.html',
      controller: "VideoDetailCtrl",
      resolve: {
        videoModel: function($stateParams, VideoCatalog) {
          return VideoCatalog.getAll().then(function(catalog){
            //console.log("video item", $stateParams.vid, catalog[$stateParams.vid])
            return catalog[$stateParams.vid];
          });
        }
      }
    })    

})


.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})