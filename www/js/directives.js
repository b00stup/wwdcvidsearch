angular.module('wwdcvideoapp.directives', ['ionic', "ui.router"])

.directive('fbIconedToggle', function(){
  return {
    restrict:"AE",
    replace: 'true',
    scope: {
      iconOn: "@",
      iconOff: "@",
      textOn: "@",
      textOff: "@",
      callbackGetState: "&",
      callbackOn: "&",
      callbackOff: "&"
    },
    templateUrl:"templates/iconedToggle.html",
    controller: function($scope) {
  		function updateView(isOn) {
  			$scope.icon = isOn ? $scope.iconOn : $scope.iconOff;
  			$scope.text = isOn ? $scope.textOn : $scope.textOff;
  		}
  		$scope.onTap = function() {
  			//console.log("onTap", $scope.isOn);
        var isOn = !$scope.callbackGetState();
        isOn ? $scope.callbackOn() : $scope.callbackOff();
        updateView(isOn);
  		}
  		updateView($scope.callbackGetState());
    }

  }
})