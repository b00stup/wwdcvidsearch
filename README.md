# INTRO #

Learning Apple Swift 2 is a serious affair. As it is, apple has NOT provided a way to search for a videos related to a common topic over the years.  Each year's WWDC does NOT reconcile previous years related videos on a single results page.

So, this app intends to ease this process with a very helpful search form.  With it, if I want to know anything about, let's say, the GAME CENTER framework, than I can see all related videos, over the many years the WWDC has run, and all in the same results page.  

Good luck achieving this on the official website WWDC website (https://developer.apple.com/videos/play/wwdc2012/508/)[WWDC website] !!!

This an Ionic Framework app.

# ROADMAP #

## Current State ##

- android version:  on android, the tabs on are on top, which is fine, but the space where the tabs are on iOS is still there...

	- I have created a question on stackoverflow.  Have already received an answer pointing at the fact that my html structure is incorrect.

	- After analysis:
	
		- this has a serious **impact on the router** config, and the overall HTML structure of the app which I'm going to have to rewrite.  
		- I also noticed that I have an **unused template called tabs.html**
		- I need to replace the content of **tabs.html** with the tabs structure found in the **index.html**

- inAppBrowser plugin is installed, but the VideoDetailCtrl calls 'window.open()', instead of 'cordova.InAppBrowser.open()'

- try removing the keyboard toolbar on the search form, doc found at: http://ngcordova.com/docs/plugins/keyboard/.  Even tough the app.js calls '$cordovaKeyboard.hideAccessoryBar(true)', the keyboard toolbar is there...

- rewrite functions signatures for Dependency Injection

- integrate some cloud (FireBase, Parse??) to store the video catalog metadata such as "favorites" and viewed" video id's


## Nice to have ##

- the search critera panel : 
	- animate the appearance/dissapearance of tags within the panel

- the detail view:
	- animate when 'like' or 'viewed' button is clicked

- the list view: 
    - use slide to reveal on rows so that video can be placed into favorites or viewed right from the list view
    - change the template code so that sorting happens only once all filtering has been done
    - toogle between view templates for showing a simple list ( as it currently is), or a horizontal set of cards (reusing the "detail" template, and looking at http://blog.ionic.io/ionic-swipeable-cards/)

## Tried, without success ##

- have been able to install on iOS device from ionic CLI ~~install app on iOS device using the ionci CLI (i have to open the generated Xcode project to install the app, which is not good, since this file is not even versionned in the GIT repository; it is ignored)~~

- ~~rewrite all $scopes so that all vars are on "$scope.data" and not directly on $scope.~~ this seems to have the caveat that suddenly boilerplate code is introduced in every controller, ie: "$scope.data = {};"

- ~~rewrite all $scopes so that functions are passed via "Controller as ctrl" rather then adding the function on the $scope itself the controllerAs="ctrl" field of the ui-router state definition.~~  Seems not to be working: I never was able to call ctrl.someVar or ctrl.someFn from the template file.  ctrl was always undefined...

## might not be that useful ##

- main vue table:  integrate headers to avoid having to repeat the category or year on each item of the list (header expected behaviour as iOS headers is not doable out of the box)
- making a fake loading transition right after seach form modal is closed
- try using an actionsheet instead of a modal to show the search form
- give user a way to change the direction of the table sorting (ASC | DESC) for criteria: year, cat, session, title